// https://discordapp.com/oauth2/authorize?client_id=291650980578197506&scope=bot

/*jshint
esversion: 6
*/
const Discord = require('discord.io');
const fs = require('fs');
const os = require('os');
const http = require('http');
const auth = require('http-auth');
const querystring = require('querystring');
const request = require('request');

var basic = auth.basic({
    realm: "Simon Area.",
    file: __dirname + "/users.htpasswd"
});

var configuration = require(__dirname + '/configuration.json');
function reloadConfiguration() {
  delete require.cache[require.resolve(__dirname + '/configuration.json')];
  require(__dirname + '/configuration.json');
}

const bot = new Discord.Client({
  token: configuration.apiKeys.discord.release,
  autorun: true
});

bot.on('ready', function() {
    console.log('Logged in as %s - %s\n', bot.username, bot.id);
    bot.setPresence({
      game: {
        name: configuration.prefix + 'help'
      }
    });
    request('https://discordbots.org/api/bots/291650980578197506', function(err, res, data) {
      bot.editMessage({
        channelID: '307663067389493248',
        messageID: '307847620850876418',
        message: 'DiscordBots.org points: ' + JSON.parse(data).points
      });
    });
});

bot.on('message', function(user, userID, channelID, message, event) {
  try {
    console.log(bot.servers[bot.channels[channelID].guild_id].name + ' (' + bot.channels[channelID].guild_id + ') #' + bot.channels[channelID].name + ' (' + channelID + ') ' + user + ' (' + userID + '): ' + message);
  } catch (error) {
    console.log(error);
  }
  if (bot.users[userID] !== undefined) {
    if (bot.users[userID].bot === false) {
    if (message.startsWith(configuration.prefix + 'ping')) {
      bot.sendMessage({
        to: channelID,
        message: 'pong'
      });
    }
    else if (message.startsWith(configuration.prefix + 'invite')) {
      bot.sendMessage({
        to: channelID,
        message: '<https://discordapp.com/oauth2/authorize?client_id=291650980578197506&scope=bot>'
      });
    }
    else if (message.startsWith(configuration.prefix + 'testbot')) {
      bot.sendMessage({
        to: channelID,
        message: 'I work, I really work!'
      });
    }
    else if (message.startsWith(configuration.prefix + 'help')) {
      bot.sendMessage({
        to: channelID,
        message: `\
Commands:

${configuration.prefix}cat - Send a picture of a cat
${configuration.prefix}rev - Reverse a message
${configuration.prefix}help - Show this message
${configuration.prefix}invite - Invite NanoBot to your server
${configuration.prefix}say - Say a message
${configuration.prefix}testbot - Test if the bot is working
${configuration.prefix}info - Server count, user count, uptime, RAM usage
`
      });
    }
    else if (message.startsWith(configuration.prefix + 'rev')) {
      var revString = message.substr(configuration.prefix.length + 3);
      bot.sendMessage({
        to: channelID,
        message: revString.split('').reverse().join('')
      });
    }
    else if (message.startsWith(configuration.prefix + 'say')) {
      var sayString = message.substr(configuration.prefix.length + 3);
      bot.sendMessage({
        to: channelID,
        message: sayString
      });
    }
    else if (message.startsWith(configuration.prefix + 'cat')) {
      bot.uploadFile({
        to: channelID,
        file: 'cats/cats_cats_' + Math.floor((Math.random() * 100) + 1) + '.jpg'
      });
    }
    else if (message.startsWith(configuration.prefix + 'info')) {
      bot.sendMessage({
        to: channelID,
        embed: {
          title: 'Info',
          fields: [
            {
              name: 'Process Memory Usage',
              value: Math.round(process.memoryUsage().rss / (1024 * 1024)) + 'MB',
              inline: true
            },
            {
              name: 'Servers',
              value: String(Object.keys(bot.servers).length),
              inline: true
            },
            {
              name: 'Members',
              value: String(Object.keys(bot.users).length),
              inline: true
            }
          ],
          color: 0xFF000
        }
      });
    }
    else if (message.startsWith(configuration.prefix + 'broadcast') && userID === '235804673578237952') {
      broadcast(message.substr(configuration.prefix.length + 9));
    }
    else if (message.startsWith(configuration.prefix + 'reconnect') && userID === '235804673578237952') {
      bot.sendMessage({
        to:channelID,
        message: 'Reconnecting...'
      });
      bot.disconnect();
    }
  }
  }
});

function broadcast(message) {
  var i = 0;
  for (var serverProp in bot.servers) {
    i += 1;
    if (i < 50) {
      bot.sendMessage({
        to: serverProp,
        message: message
      });
    }
  }
}

bot.on('disconnect', function(msg, code) {
    if (code === 0) return console.error(msg);
    bot.connect();
});

http.createServer(basic,
  function(request, response) {
    response.writeHead(200, {
      "Content-Type": "text/html"
    });
    if (request.url === '/') {
      response.write(`
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>
      NanoBot
    </title>
  </head>
  <body>
    <a href="/users">Users</a>
    <a href="/servers">Servers</a>
  </body>
</html>
`);
    }
    if(request.url == '/users') {
      response.write(`
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>
      NanoBot
    </title>
    <style>
      * {
        box-sizing: border-box;
      }

      #myInput {
        background-image: url('https://w3schools.com/css/searchicon.png');
        background-position: 10px 10px;
        background-repeat: no-repeat;
        width: 100%;
        font-size: 16px;
        padding: 12px 20px 12px 40px;
        border: 1px solid #ddd;
        margin-bottom: 12px;
      }

      #myTable {
        border-collapse: collapse;
        width: 100%;
        border: 1px solid #ddd;
        font-size: 18px;
      }

      #myTable th, #myTable td {
        text-align: left;
        padding: 12px;
      }

      #myTable tr {
        border-bottom: 1px solid #ddd;
      }

      #myTable tr.header, #myTable tr:hover {
        background-color: #f1f1f1;
      }
    </style>
  </head>
  <body>
    <a href="/" style="text-decoration: none;color: lightblue"><h1 style="display: inline;"><-</h1></a>
    <h1 style="display: inline;">Users</h1>
    <hr>
    <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search" title="Type in a name">
    Search by:
    <select id="searchType">
      <option value="0">Names</option>
      <option value="1">Bot</option>
      <option value="2">Discriminator</option>
      <option value="3">ID</option>
    </select>
    <br>
    <table id="myTable">
      <tr class="header">
        <th>Name</th>
        <th>Bot</th>
        <th>Discriminator</th>
        <th>ID</th>
      </tr>
`);
      var members = bot.users;
      for (var prop in members) {
        if (members[prop].username !== undefined && members[prop].bot !== undefined && members[prop].discriminator !== undefined && members[prop].discriminator !== undefined) {
          response.write('<tr><td>' + members[prop].username.replace('<', '&lt;').replace('>', '&gt;') + '</td><td>' + String(members[prop].bot).charAt(0).toUpperCase() + String(members[prop].bot).slice(1) + '</td><td>' + members[prop].discriminator + '</td><td>' + members[prop].id + '</td></tr>');
        }
      }
    response.write(`
    </table>
    <script>
      function myFunction() {
        var input, filter, table, tr, td, i, col;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        col = document.getElementById('searchType').value;
        for (i = 0; i < tr.length; i++) {
          td = tr[i].getElementsByTagName("td")[col];
          if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
              tr[i].style.display = "";
            } else {
              tr[i].style.display = "none";
            }
          }
        }
      }
    </script>
  </body>
</html>
`);
    }
    if(request.url == '/servers') {
      response.write(`
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>
      NanoBot
    </title>
    <style>
      * {
        box-sizing: border-box;
      }

      #myInput {
        background-image: url('https://w3schools.com/css/searchicon.png');
        background-position: 10px 10px;
        background-repeat: no-repeat;
        width: 100%;
        font-size: 16px;
        padding: 12px 20px 12px 40px;
        border: 1px solid #ddd;
        margin-bottom: 12px;
      }

      #myTable {
        border-collapse: collapse;
        width: 100%;
        border: 1px solid #ddd;
        font-size: 18px;
      }

      #myTable th, #myTable td {
        text-align: left;
        padding: 12px;
      }

      #myTable tr {
        border-bottom: 1px solid #ddd;
      }

      #myTable tr.header, #myTable tr:hover {
        background-color: #f1f1f1;
      }
    </style>
  </head>
  <body>
    <a href="/" style="text-decoration: none;color: lightblue"><h1 style="display: inline;"><-</h1></a>
    <h1 style="display: inline;">Servers</h1>
    <hr>
    <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search" title="Type in a name">
    Search by:
    <select id="searchType">
      <option value="0">Names</option>
      <option value="1">Locations</option>
      <option value="2">Verification Levels</option>
      <option value="3">Unavailable?</option>
    </select>
    <br>
    <table id="myTable">
      <tr class="header">
        <th>Name</th>
        <th>Location</th>
        <th>Verification Level</th>
        <th>Unavailable</th>
      </tr>
`);
      var servers = bot.servers;
      for (var serverProp in servers) {
        if (servers[serverProp].name !== undefined && servers[serverProp].region !== undefined && servers[serverProp].verification_level !== undefined && servers[serverProp].unavailable !== undefined) {
          response.write('<tr><td>' + servers[serverProp].name.replace('<', '&lt;').replace('>', '&gt;') + '</td><td>' + servers[serverProp].region.replace('-', ' ').charAt(0).toUpperCase() + servers[serverProp].region.replace('-', ' ').slice(1) + '</td><td>' + String(servers[serverProp].verification_level) + '</td><td>' + String(servers[serverProp].unavailable) + '</td></tr>');
        }
      }
    response.write(`
    </table>
    <script>
      function myFunction() {
        var input, filter, table, tr, td, i, col;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        col = document.getElementById('searchType').value;
        for (i = 0; i < tr.length; i++) {
          td = tr[i].getElementsByTagName("td")[col];
          if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
              tr[i].style.display = "";
            } else {
              tr[i].style.display = "none";
            }
          }
        }
      }
    </script>
  </body>
</html>
`);
    }
    if (request.url.startsWith('/broadcast?')) {
      var query = querystring.parse(request.url.split('?')[1]);
      broadcast(String(query.message));
    }
    response.end();
  }
).listen(8888);
