# https://discordapp.com/oauth2/authorize?client_id=291650980578197506&scope=bot&permissions=3072

# Import modules
print('Importing modules')
import _thread as thread
import cherrypy
import binascii
import requests
import discord
import asyncio
import dpaste
import random
import psutil
import sys
import os

# Import commands
print('Importing commands')
from commands.invite import *

admins = ['235804673578237952']
banned = []
no_mass_message = ['110373943822540800', '239772188649979904', '293743621684068353']

print('Fetching filter list')
badwordsfile = requests.get('https://raw.githubusercontent.com/LDNOOBW/List-of-Dirty-Naughty-Obscene-and-Otherwise-Bad-Words/master/en').text
badwords = badwordsfile.split('\n')

client = discord.Client()
prefix = '\\'

users = 0
uptime = 0
servers = 0
servernames = []

async def one_second_background_task():
    global uptime
    while True:
        uptime += 1
        await asyncio.sleep(1)

async def ten_minute_background_task():
    global users
    global servers
    global peoplenames
    global servernames
    await client.wait_until_ready()
    while True:
        people = []
        peoplenames = []
        users = 0
        servers = 0
        servernames = []
        for server in client.servers:
            servernames += [server.name]
            servers += 1
            try:
                for member in server.members:
                    if not member.bot:
                        if not member.id in people:
                            if str(member.status) == 'online':
                                users += 1
                                people += [member.id]
                                peoplenames += [member.name]
            except:
                pass
        await client.change_presence(game=discord.Game(name='with ' + str(users) + ' people | ' + prefix + 'help'))
        await asyncio.sleep(60 * 10)
def restart_program():
    python = sys.executable
    os.execl(python, python, * sys.argv)
async def send_msg(channel, message, errmessage='Error - Bad Words'):
    for item in badwords:
        if item.lower() in message.lower():
            if item != '' and item.lower() != 'hell':
                await client.send_message(channel, errmessage)
                return ''
    await client.send_message(channel, message)
    return ''
def is_ascii(s):
    return all(ord(c) < 128 for c in s)

@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')
@client.event
async def on_message(message):
    global users
    global uptime
    global peoplenames
    global servers
    global servernames
    msg = message.content.lower()
    if not message.author.bot:
        if message.author.id in banned:
            if message.content.startswith(prefix):
                await client.send_message('You have been banned from this bot.')
                return ''
        if msg == prefix + 'invite':
            await invite(client, message)
        elif msg == prefix + 'testbot':
            await send_msg(message.channel, 'I work, I really work!')
        elif msg == prefix + 'help':
            await client.send_message(message.channel, prefix + 'cat - Send a picture of a cat\n' + prefix + 'rev - Reverse a message\n' + prefix + 'help - Show this message\n' + prefix + 'invite - Invite NanoBot to your server\n' + prefix + 'say - Say a message\n' + prefix + 'testbot - Test if the bot is working\n' + prefix + 'info - Server count, user count, uptime, RAM usage')
        elif msg.startswith(prefix + 'rev'):
            await send_msg(message.channel, message.content[4 + len(prefix):][::-1])
        elif msg == prefix + 'cat':
            await client.send_file(message.channel, 'cats/cats_cats_' + str(random.randint(1, 100)) + '.jpg')
        elif msg == prefix + 'info':
            time = uptime
            day = time // (24 * 3600)
            time = time % (24 * 3600)
            hour = time // 3600
            time %= 3600
            minutes = time // 60
            time %= 60
            seconds = time
            await send_msg(message.channel, '```Servers: ' + str(servers) + '\nPeople: ' + str(users) + '\nUptime: ' + str(day) + ' days, ' + str(hour) + ' hours, ' + str(minutes) + ' minutes, and ' + str(seconds) + ' seconds\nRAM: ' + str(psutil.virtual_memory().percent) + '%```\nSay ' + prefix + 'servers to list what servers I\'m on.' )
        elif msg.startswith(prefix + 'say'):
            await send_msg(message.channel, message.content[4 + len(prefix):], 'I refuse to say that!')
        elif msg.startswith(prefix + 'broadcast'):
            if message.author.id in admins:
                i = 0
                for server in client.servers:
                    if i < 50:
                        if not server.id in no_mass_message:
                            try:
                                await send_msg(server.default_channel, message.content[10 + len(prefix):])
                            except:
                                pass
                            i += 1
                    else:
                        await asyncio.sleep(10)
                        i = 0
                        try:
                            await send_msg(server.default_channel, message.content[12 + len(prefix):])
                        except:
                            pass
            else:
                try:
                    await send_msg(message.channel, "You aren't an admin.")
                except:
                    pass
        elif msg == prefix + 'restart':
            if message.author.id in admins:
                try:
                    await send_msg(message.channel, 'Restarting...')
                except:
                    pass
                restart_program()
            else:
                try:
                    await send_msg(message.channel, "You aren't an admin.")
                except:
                    pass
        elif msg == prefix + 'servers':
            try:
                location = dpaste.post('\n'.join(servernames))
                await send_msg(message.channel, location)
            except:
                pass
        elif msg == prefix + 'people':
            if message.author.id in admins:
                try:
                    await send_msg(message.channel, 'Sending to DMs...')
                except:
                    pass
                peoplenames.sort()
                location = dpaste.post('\n'.join(peoplenames))
                await send_msg(message.author, location)
        elif msg.startswith(prefix + 'ascii2binary'):
            asciicode = message.content[14:]
            await send_msg(message.channel, str(text_to_bits(asciicode)))

def text_to_bits(text, encoding='utf-8', errors='surrogatepass'):
    bits = bin(int(binascii.hexlify(text.encode(encoding, errors)), 16))[2:]
    to_return = bits.zfill(8 * ((len(bits) + 7) // 8))
    to_return = [to_return[i:i+8] for i in range(0, len(to_return), 8)]
    to_return = ' '.join(to_return)
    return to_return

def text_from_bits(bits, encoding='utf-8', errors='surrogatepass'):
    n = int(bits, 2)
    return int2bytes(n).decode(encoding, errors)

def int2bytes(i):
    hex_string = '%x' % i
    n = len(hex_string)
    return binascii.unhexlify(hex_string.zfill(n + (n & 1)))

def start_server():
    print('Hoi')

print('Starting background tasks')
client.loop.create_task(ten_minute_background_task())
client.loop.create_task(one_second_background_task())
thread.start_new_thread(start_server, ())
print('Starting main client')
client.run('MjkxNjUwOTgwNTc4MTk3NTA2.C6sk5g.2i8MXYEmLkQ72SZhKugriTNrTes')
