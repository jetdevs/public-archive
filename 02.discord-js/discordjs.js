// https://discordapp.com/oauth2/authorize?client_id=291650980578197506&scope=bot

/*jshint
esversion: 6
*/
const Discord = require('discord.js');
const fs = require('fs');
const http = require('http');
const auth = require('http-auth');
const querystring = require('querystring');
const request = require('request');
const mysql = require('mysql');

var basic = auth.basic({
    realm: "Simon Area.",
    file: __dirname + "/users.htpasswd"
});

var configuration = require(__dirname + '/configuration.json');
var package = require(__dirname + '/package.json');
function reloadConfiguration() {
  delete require.cache[require.resolve(__dirname + '/configuration.json')];
  require(__dirname + '/configuration.json');
}

const client = new Discord.Client();

client.on('ready', function() {
    updateServerDB();
    console.log('Logged in as %s - %s\n', client.user.username, client.user.id);
    client.user.setPresence({
      game: {
        name: configuration.prefix + 'help'
      }
    });
    client.guilds.get('307572127932874752').channels.get('308926406585483266').send('Started.');
});

var connection = mysql.createConnection({
  host: "localhost",
  user: "REDACTED",
  password: "REDACTED",
  database: 'nanobot'
});
connection.connect(function(err) {
  if (err) throw err;
});
var serv3rs;

function updateServerDB() {
  console.log('0');
  serv3rs = client.guilds.array();
  for (i = 0; i < client.guilds.size - 1; i++) {
    console.log(serv3rs[i].id);
    connection.query(`
select * from servers
where id=${serv3rs[i].id}
`, function(err, result) {
      if (err) throw err;
      if (result.length > 0) {
        connection.query(`
update servers
set id = ${serv3rs[i].id}, name = '${serv3rs[i].name}', members = ${serv3rs[i].memberCount} verification = ${serv3rs[i].verificationLevel}, broadcasts = 0
where id=${serv3rs[i].id};
`, function(err, result) {
          if (err) throw err;
        });
      }
      else {
        connection.query(`
insert into servers
values ('${serv3rs[i].id}', '${serv3rs[i].name}', ${serv3rs[i].memberCount}, ${serv3rs[i].verificationLevel}, 0);
`, function(err, result) {
          if (err) throw err;
        });
      }
    });
  }
}

function updateInfo() {
  request('https://discordbots.org/api/bots/291650980578197506', function(err, res, data) {
    var discordListDotOrgPoints = JSON.parse(data).points;
  }, function(error, response, data) {
    request({
      options: {
        method: 'post',
        token: "n1W6wqvOH8", // KklveoDHd4, cEZE4qSn2M
        servers: client.guilds.size,
        url: 'https://bots.discordlist.net/api.php',
        json: true
      }
    }, function(err,res, body) {
      client.guilds.get("307572127932874752").channels.get("307663067389493248").fetchMessage("307847620850876418").then(msg=>msg.edit('DiscordBots.org points: ' + discordListDotOrgPoints + ' - Vote at <https://discordbots.org/bot/291650980578197506>'));
    });
  });
}

client.on('message', message => {
  if (message.author.bot === false) {
    if (message.content.startsWith(configuration.prefix + 'ping')) {
      message.channel.send('pong - ' + Math.round(client.ping) + 'ms');
    }
    else if (message.content.startsWith(configuration.prefix + 'invite')) {
      message.channel.send('<https://discordapp.com/oauth2/authorize?client_id=291650980578197506&scope=bot>');
    }
    else if (message.content.startsWith(configuration.prefix + 'testbot')) {
      message.channel.send('I work, I really work!');
    }
    else if (message.content.startsWith(configuration.prefix + 'help')) {
      message.channel.send(`
Commands:

${configuration.prefix}cat - Send a picture of a cat
${configuration.prefix}rev - Reverse a message
${configuration.prefix}help - Show this message
${configuration.prefix}invite - Invite NanoBot to your server
${configuration.prefix}say - Say a message
${configuration.prefix}testbot - Test if the bot is working
${configuration.prefix}info - Server count, user count, uptime, RAM usage
`);
    }
    else if (message.content.startsWith(configuration.prefix + 'rev')) {
      var revString = message.content.substr(configuration.prefix.length + 3);
      message.channel.send(revString.split('').reverse().join(''));
    }
    else if (message.content.startsWith(configuration.prefix + 'say')) {
      var sayString = message.content.substr(configuration.prefix.length + 3);
      message.channel.send(sayString);
    }
    else if (message.content.startsWith(configuration.prefix + 'cat')) {
      message.channel.sendFile('cats/cats_cats_' + Math.floor((Math.random() * 100) + 1) + '.jpg');
    }
    else if (message.content.startsWith(configuration.prefix + 'info')) {
      message.channel.send({
        embed: {
          title: 'Info',
          fields: [
            {
              name: 'Process Memory Usage',
              value: Math.round(process.memoryUsage().rss / (1024 * 1024)) + 'MB',
              inline: true
            },
            {
              name: 'Servers',
              value: String(client.guilds.size),
              inline: true
            },
            {
              name: 'Members',
              value: String(client.users.size),
              inline: true
            },
            {
              name: 'Language',
              value: 'Node v' + process.versions.node,
              inline: true
            },
            {
              name: 'Library',
              value: 'Discord.JS v' + package.dependencies['discord.js'],
              inline: true
            },
            {
              name: 'Creator',
              value: '<@235804673578237952>',
              inline: true
            },
            {
              name: 'Support Server',
              value: 'https://discord.gg/jVFTNd4',
              inline: true
            }
          ],
          color: 0xFF000
        }
      });
    }
    else if (message.content.startsWith(configuration.prefix + 'broadcast') && userID === '235804673578237952') {
      broadcast(message.substr(configuration.prefix.length + 9));
    }
    else if (message.content.startsWith(configuration.prefix + 'disconnect') && userID === '235804673578237952') {
      message.channel.send('Disconnecting...');
      client.disconnect();
    }
    if (message.content.toLowerCase().startsWith(configuration.prefix + 'eval') && message.author.id === '235804673578237952') {
      try {
        message.channel.send(':inbox_tray: Input:```js\n' + message.content.substr(configuration.prefix.length + 5) + '\n```:outbox_tray: Output:```js\n' + eval(message.content.substr(configuration.prefix.length + 5)) + '\n```');
      }
      catch (e) {
        message.channel.send(':inbox_tray: Input:```js\n' + message.content.substr(configuration.prefix.length + 5) + '\n```:outbox_tray: Output:```js\n' + e + '\n```');
      }
    }
    if (message.content.toLowerCase().startsWith(configuration.prefix + 'run') && message.author.id === '235804673578237952') {
      eval(message.content.substr(configuration.prefix.length + 4));
    }
    else if (message.content.toLowerCase().startsWith(configuration.prefix + 'stop') && message.author.id === '235804673578237952') {
      message.channel.send('Stopping.', () => {
        client.guilds.get('307572127932874752').channels.get('308926406585483266').send('Stopping.', () => {
          process.exit();
        });
      });
    }
  }
});

function broadcast(message) {
  var i = 0;
  for (var serverProp in client.guilds) {
    i += 1;
    if (i < 50) {
      client.guilds[serverProp].client.guilds[serverProp].send(message);
    }
  }
}

http.createServer(basic,
  function(request, response) {
    response.writeHead(200, {
      "Content-Type": "text/html"
    });
    if (request.url === '/') {
      response.write(`
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>
      NanoBot
    </title>
  </head>
  <body>
    <a href="/users">Users</a>
    <a href="/servers">Servers</a>
  </body>
</html>
`);
    }
    if(request.url == '/users') {
      response.write(`
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>
      NanoBot
    </title>
    <style>
      * {
        box-sizing: border-box;
      }

      #myInput {
        background-image: url('https://w3schools.com/css/searchicon.png');
        background-position: 10px 10px;
        background-repeat: no-repeat;
        width: 100%;
        font-size: 16px;
        padding: 12px 20px 12px 40px;
        border: 1px solid #ddd;
        margin-bottom: 12px;
      }

      #myTable {
        border-collapse: collapse;
        width: 100%;
        border: 1px solid #ddd;
        font-size: 18px;
      }

      #myTable th, #myTable td {
        text-align: left;
        padding: 12px;
      }

      #myTable tr {
        border-bottom: 1px solid #ddd;
      }

      #myTable tr.header, #myTable tr:hover {
        background-color: #f1f1f1;
      }
    </style>
  </head>
  <body>
    <a href="/" style="text-decoration: none;color: lightblue"><h1 style="display: inline;"><-</h1></a>
    <h1 style="display: inline;">Users</h1>
    <hr>
    <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search" title="Type in a name">
    Search by:
    <select id="searchType">
      <option value="0">Names</option>
      <option value="1">Bot</option>
      <option value="2">Discriminator</option>
      <option value="3">ID</option>
    </select>
    <br>
    <table id="myTable">
      <tr class="header">
        <th>Name</th>
        <th>Bot</th>
        <th>Discriminator</th>
        <th>ID</th>
      </tr>
`);
      members = client.users.array();
      for (var prop in members) {
        if (members[prop].username !== undefined && members[prop].bot !== undefined && members[prop].discriminator !== undefined && members[prop].discriminator !== undefined) {
          response.write('<tr><td>' + members[prop].username + '</td><td>' + String(members[prop].bot).charAt(0).toUpperCase() + String(members[prop].bot).slice(1) + '</td><td>' + members[prop].discriminator + '</td><td>' + members[prop].id + '</td></tr>');
        }
      }
    response.write(`
    </table>
    <script>
      function myFunction() {
        var input, filter, table, tr, td, i, col;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        col = document.getElementById('searchType').value;
        for (i = 0; i < tr.length; i++) {
          td = tr[i].getElementsByTagName("td")[col];
          if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
              tr[i].style.display = "";
            } else {
              tr[i].style.display = "none";
            }
          }
        }
      }
    </script>
  </body>
</html>
`);
    }
    if(request.url == '/servers') {
      response.write(`
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>
      NanoBot
    </title>
    <style>
      * {
        box-sizing: border-box;
      }

      #myInput {
        background-image: url('https://w3schools.com/css/searchicon.png');
        background-position: 10px 10px;
        background-repeat: no-repeat;
        width: 100%;
        font-size: 16px;
        padding: 12px 20px 12px 40px;
        border: 1px solid #ddd;
        margin-bottom: 12px;
      }

      #myTable {
        border-collapse: collapse;
        width: 100%;
        border: 1px solid #ddd;
        font-size: 18px;
      }

      #myTable th, #myTable td {
        text-align: left;
        padding: 12px;
      }

      #myTable tr {
        border-bottom: 1px solid #ddd;
      }

      #myTable tr.header, #myTable tr:hover {
        background-color: #f1f1f1;
      }
    </style>
  </head>
  <body>
    <a href="/" style="text-decoration: none;color: lightblue"><h1 style="display: inline;"><-</h1></a>
    <h1 style="display: inline;">Servers</h1>
    <hr>
    <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search" title="Type in a name">
    Search by:
    <select id="searchType">
      <option value="0">Names</option>
      <option value="1">Locations</option>
      <option value="2">Verification Levels</option>
      <option value="3">Available?</option>
    </select>
    <br>
    <table id="myTable">
      <tr class="header">
        <th>Name</th>
        <th>Location</th>
        <th>Verification Level</th>
        <th>Available</th>
      </tr>
`);
      var servers = client.guilds.array();
      for (var serverProp in servers) {
        if (servers[serverProp].name !== undefined && servers[serverProp].region !== undefined && servers[serverProp].verificationLevel !== undefined && servers[serverProp].available !== undefined) {
          response.write('<tr><td>' + servers[serverProp].name + '</td><td>' + servers[serverProp].region.replace('-', ' ').charAt(0).toUpperCase() + servers[serverProp].region.replace('-', ' ').slice(1) + '</td><td>' + String(servers[serverProp].verificationLevel) + '</td><td>' + String(servers[serverProp].available) + '</td></tr>');
        }
      }
    response.write(`
    </table>
    <script>
      function myFunction() {
        var input, filter, table, tr, td, i, col;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        col = document.getElementById('searchType').value;
        for (i = 0; i < tr.length; i++) {
          td = tr[i].getElementsByTagName("td")[col];
          if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
              tr[i].style.display = "";
            } else {
              tr[i].style.display = "none";
            }
          }
        }
      }
    </script>
  </body>
</html>
`);
    }
    if (request.url.startsWith('/broadcast?')) {
      var query = querystring.parse(request.url.split('?')[1]);
      broadcast(String(query.message));
    }
    response.end();
  }
).listen(8888);

client.login(configuration.apiKeys.discord.release);
